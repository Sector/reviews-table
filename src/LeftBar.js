import React, { Component } from "react";
import styled from "styled-components";

import { LinkText, LinkContainer } from './TopBar'

const LeftBarStyled = styled.div`
  background: #1B1C26;
  display: flex;
  width: 15%;
  flex-wrap: wrap;
  align-items: flex-start;
  align-content: flex-start;
`;

const LinkContainerLeft = styled(LinkContainer)`
  flex: 1 100%;
`

const links = [
  {
    img: '',
    path: 'Dashboard'
  },
  {
    img: '',
    path: 'Campaigns'
  },
  {
    img: '',
    path: 'Workflows'
  },
  {
    img: '',
    path: 'Templates'
  }
]

class LeftBar extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return <LeftBarStyled>
      {links.map(link => <LinkContainerLeft>
        <LinkText>
          {link.path}
        </LinkText>
      </LinkContainerLeft>)}
    </LeftBarStyled>
  }
}

export default LeftBar
