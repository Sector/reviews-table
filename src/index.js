import React from "react";
import ReactDOM from "react-dom";
import styled, { injectGlobal } from "styled-components";

import TopBar from "./TopBar";
import LeftBar from "./LeftBar";
import ReviewsTable from "./ReviewsTable";

injectGlobal`
  .App {
    font-family: sans-serif;
    text-align: center;
  }

  html,
  body {
    margin: 0px;
    height: 100%;
    background: black;
  }
`

const Page = styled.div`
  display: flex;
  flex-flow: row wrap;
  background: black;
  color: grey;
`;

function App() {
  return (
    <Page>
      <TopBar />
      <LeftBar />
      <ReviewsTable />
    </Page>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
