import React, { Component } from "react";
import styled from "styled-components";

import { SelectField, EditField, RowBody } from './ReviewsTable'

export const TableHeaderStyled = styled.div`
  margin: 10px;
  flex: 1 auto;
  display: flex;
  justify-content: space-between;
`;

export const ColumnStyled = styled.div`
  width: 15%;
`

const Arrow = styled.div`
  margin-top: 5px;
  width: 0; 
  height: 0; 
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
 `

const ArrowUp = styled(Arrow)`
  border-bottom: 5px solid mediumorchid;
`

const ArrowDown = styled(Arrow)`
  border-top: 5px solid mediumorchid;
 `

export const ColumnHeaderStyled = styled(ColumnStyled)`
  display: flex;
  cursor: pointer;
`

class TableHeader extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let headers = []
    for (let key of this.props.columns) {
      let isSelected = (this.props.orderBy.by === key) ? true : false

      headers.push(<ColumnHeaderStyled 
        key={key}
        isSelected = {isSelected}
        onClick={() => this.props.onColumnHeaderClick(key)}>
        {key}
        {isSelected && (this.props.orderBy.asc ? <ArrowDown /> : <ArrowUp />)}
      </ColumnHeaderStyled>)
    }

    return (
      <TableHeaderStyled>
        <SelectField />
          <RowBody>
            {headers}
          </RowBody>
        <EditField />
      </TableHeaderStyled>
    );
  }
}

export default TableHeader;
