import React, { Component } from "react";

import { TableRow, SelectField, RowBodyBordered, 
    EditField, EditFieldCircle, EditingRow } from './ReviewsTable'
import { ColumnStyled, } from "./TableHeader";

class TableRows extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        let reviews = this.props.reviews
        let orderBy = this.props.orderBy

        if (this.props.orderBy.by)
        {
            reviews.sort((a, b) => {
                try {
                    if (typeof a[orderBy.by] === 'string')
                        return orderBy.asc ? a[orderBy.by] > b[orderBy.by] : a[orderBy.by] < b[orderBy.by]
                    else if (typeof a[orderBy.by] === 'number')
                        return orderBy.asc ? a[orderBy.by] - b[orderBy.by] : b[orderBy.by] - a[orderBy.by]
                    else if (typeof a[orderBy.by] === 'object')
                        return orderBy.asc ? new Date(a[orderBy.by]) - new Date(b[orderBy.by]) : new Date(b[orderBy.by]) - new Date(a[orderBy.by])
                } catch (e) {
                    console.error(e)
                }

            })
        }

        const rows = this.props.reviews.map(review => {
            let rowData = []
            for (let key in review)
            {
                if (key === 'id')
                continue
                let val = review[key]
                if (typeof val === 'object') {
                    // Format date object if possible, else fallback to string
                    try {
                        let dateOptions = {year: 'numeric', month: 'long', day: 'numeric' };
                        let date = new Date(review[key]).toLocaleDateString("en-US", dateOptions)
                        if (date !== 'Invalid Date')
                            val = date  
                    } catch(e) {
                        console.log(e)
                    }
                }
                rowData.push(<ColumnStyled key={key}>{val}</ColumnStyled>)
            }

            return (
                <TableRow>
                    <SelectField 
                        isSelected={this.props.selectedRowsIds.includes(review.id)} 
                        onClick={() => this.props.handleSelectClick(review.id)} 
                    />
                    <RowBodyBordered
                        isSelected={this.props.selectedRowsIds.includes(review.id)}>
                        {rowData}
                    </RowBodyBordered>
                    <EditField onClick={() => this.props.handleEditClick(review.id)}>
                        <EditFieldCircle />
                        <EditFieldCircle />
                        <EditFieldCircle />
                    </EditField>
                    {review.id === this.props.editingRowId && <EditingRow>
                        <div>
                            <div>Activate</div>
                            <div>Preview</div>
                            <div>Edit</div>
                            <div>Stats</div>
                            <div>Delete</div>
                        </div>
                    </EditingRow>}
                </TableRow>
            )
        });
        return rows
    }
}

export default TableRows