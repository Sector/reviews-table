import React, { Component } from "react";
import styled from "styled-components";

export const TopBarStyled = styled.div`
  flex: 1 100%;
  display: flex;
  background: #2F3343;
`;

export const LinkContainer = styled.div`
  display: flex;
  flex: 0 1 auto;
  padding: 20px;
`

export const LinkImage = styled.div`

`

export const LinkText = styled.a`
  cursor: pointer
`

const links = [
  {
    img: '',
    path: 'Help'
  },
  {
    img: '',
    path: 'Clever Store'
  },
  {
    img: '',
    path: 'Other'
  }
]

class TopBar extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return <TopBarStyled>
      {links.map(link => <LinkContainer>
        <LinkText>
          {link.path}
        </LinkText>
      </LinkContainer>)}
    </TopBarStyled>
  }
}

export default TopBar
