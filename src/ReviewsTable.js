import React, { Component } from "react";
import styled from "styled-components";
import v4 from "node-uuid";

import TableHeader from "./TableHeader";
import TableRows from './TableRows';
import { ColumnStyled, ColumnHeaderStyled } from "./TableHeader";

export const TableRow = styled.div`
  margin: 10px;
  display: flex;
  flex: 1 100%;
  flex-wrap: wrap;
  align-items: space-around;
`

export const ReviewsTableStyled = styled.div`
  background: #222633;
  margin: 15px;
  display: flex;
  flex: 1 auto;
  flex-flow: row wrap;
  width: 60%;
  height: 100%;
  padding: 10px;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  flex: 1 100%;
  div {
    display: flex;
    max-height: 50%;
  }
`

/* **** */
export const SelectField = styled.div`
  background: ${props => props.isSelected ? 'mediumorchid' : 'none'};
  border: 1px dashed mediumorchid;
  border-radius: 5px;
  width: 25px;
  height: 25px;
  margin: 5px;
  cursor: pointer;
`;

export const RowBody = styled.div`
  display: flex;
  flex: 1 auto;
  justify-content: left;
  align-items: center;
  background: ${props => props.isSelected && '#2F3343'};
  border-radius: 5px;
`

export const RowBodyBordered = styled(RowBody)`
  border: ${props => !props.isSelected && '1px darkviolet dashed'};
  border-radius: 5px;
`

export const EditField = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`;

export const EditFieldCircle = styled.div`
  border: #555555 solid 1px;
  border-radius: 50%;
  background: #555555;
  margin: 2px
  width: 5px;
  height: 5px;
`

/* **** */
export const EditingRow = styled.div`
  color: white;
  display: flex;
  width: 100%;
  justify-content: center;
  div {
    display: flex;
    width: 80%;
    background: #1B1C26;
    border-radius: 10px;
    justify-content: center;
    div {
      border-radius: 10px;
      margin: 5px;
    }

  }
`
// https://www.w3schools.com/css/css3_buttons.asp
export const Button = styled.button`
    background-color: mediumorchid; /* Green */
    border: none;
    color: white;
    padding: 11px 24px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    border-radius: 5px;
`

export const NewSurveysButton = styled(Button)`
  background-image: linear-gradient(mediumorchid, lightblue);
`


const orderBy = {
  asc: true,
  by: undefined
};

const reviews = [
  {
    id: v4(),
    Title: "Todo",
    State: "Undone",
    Viewed: 155,
    Answered: 52,
    Folder: "unikatni",
    Created: new Date("2017-05-05"),
    Valid_until: new Date("2018-05-05"),
    Created_by: "Seki",
  },
  {
    id: v4(),
    Title: "New review",
    State: "Done",
    Viewed: 55,
    Answered: 552,
    Folder: "pravidelne",
    Created: new Date("2018-01-01"),
    Valid_until: new Date("2019-02-02"),
    Created_by: "Veronika",
  },
  {
    id: v4(),
    Title: "Old review",
    State: "In progress",
    Viewed: 15,
    Answered: 252,
    Folder: "pravidelne",
    Created: new Date("2018-01-03"),
    Valid_until: new Date("2019-02-03"),
    Created_by: "Katka",
  }
];

class ReviewsTable extends Component {
  constructor(props) {
    super(props);
    let columns = []
    for (let col in reviews[0])
    {
      if (col === 'id')
        continue
      columns.push(col)
    }

    this.state = {
      columns,
      reviews,
      orderBy,
      selectedRowsIds: [],
      editingRowId: null
    }; 
  }

  handleSelectClick = (id) => {
    let selectedRowsIds = this.state.selectedRowsIds
    if (this.state.selectedRowsIds.includes(id)) {
      selectedRowsIds = selectedRowsIds.filter(reviewId => reviewId !== id)
    }
    else {
      selectedRowsIds.push(id)
    }

    this.setState({selectedRowsIds: selectedRowsIds})
  }

  handleEditClick = (id) => {
    if (this.state.editingRowId === id)
      this.setState({editingRowId: null})      
    else
      this.setState({editingRowId: id})
  }

  handleColumnHeaderClick = (key) => {
    let oldOrderBy = this.state.orderBy
    if (oldOrderBy.by === key) {
      this.setState({
        orderBy: {
          ...oldOrderBy,
          asc: !oldOrderBy.asc
        }
      })
    }
    else {
      this.setState({
        orderBy: {
          asc: true,
          by: key
        }
      })
    }
  }

  render() {
  
    // Render return
    return (
      <ReviewsTableStyled>
        <Header>
          <div>
            <h1>Surveys</h1>
          </div>
          <div>
            <NewSurveysButton>New Survey</NewSurveysButton>
          </div>
        </Header>
        <TableHeader 
          onColumnHeaderClick={this.handleColumnHeaderClick}
          columns={this.state.columns} 
          orderBy={this.state.orderBy}
        />
        <TableRows 
          orderBy={this.state.orderBy} 
          reviews={this.state.reviews} 
          selectedRowsIds={this.state.selectedRowsIds}
          editingRowId={this.state.editingRowId}
          handleSelectClick={this.handleSelectClick}
          handleEditClick={this.handleEditClick}
        />
      </ReviewsTableStyled>
    );
  }
}

export default ReviewsTable;
